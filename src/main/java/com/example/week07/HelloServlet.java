/**
 *  A Java program that starts a Tomcat Server
 *  and loads an HTML page that calls for
 *  the parameters of a cube in LxWxH.
 *  The input is then sent to a servlet that
 *  calculates the volume of the cube
 *  and prints the result to the console.
 *
 *  Author: Dainon Snow
 *  CIT360
 *  2/18/2022
 */

package com.example.week07;
import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

// Some code from Brother Tuckett tutorial
@WebServlet(name = "cubeServlet", value = "/cube-servlet")
public class HelloServlet extends HttpServlet {

    //Method to start Tomcat and get the parameters from the index page
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String inputLength = request.getParameter("length");
        int length = Integer.parseInt(inputLength);

        String inputWidth = request.getParameter("width");
        int width = Integer.parseInt(inputWidth);

        String inputHeight = request.getParameter("height");
        int height = Integer.parseInt(inputHeight);

        String inputUom = request.getParameter("uom");

        //Calculate the math
        int area = length * width * height;

        //Write the output to a blank page
        PrintWriter writer = response.getWriter();
        writer.println("<html>The volume of the cube is: " + area + " cu/" + inputUom + "</html>");
        writer.flush();

    }

    //Destroy the servlet
    public void destroy() {
    }
}
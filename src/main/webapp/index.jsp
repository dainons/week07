<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Cube Calculator</title>
</head>
<body>
<h1>Calculator to compute the volume of a cube</h1>

<! some form elements from codejava.net>

<form action="cube-servlet" method="post">
    Width: <label>
    <input type="number" size="5" name="length" placeholder="integer" required/>
</label>
    &nbsp;&nbsp;
    Width: <label>
    <input type="number" size="5" name="width" placeholder="integer" required/>
</label>
    &nbsp;&nbsp;
    Height: <label>
    <input type="number" size="5" name="height" placeholder="integer" required/>
</label>
    &nbsp;&nbsp;

 <! menu pull down from W3schools.com>

   <label for="uom">Unit of Measure:</label>
    <select name="uom" id="uom">
        <option value="in">inches</option>
        <option value="ft">feet</option>
        <option value="yd">yards</option>
        <option value="mi">miles</option>
    </select>

    <br/><br/>
    <input type="submit" value="Calculate" />
</form>
</body>
</html>